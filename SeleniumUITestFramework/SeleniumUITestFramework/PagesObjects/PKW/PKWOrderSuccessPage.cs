﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWOrderSuccessPage : MainPageOnlineSales
    {
        public PKWOrderSuccessPage(RemoteWebDriver driver) : base(driver)
        {
        }
        public void IsOrderSuccessPageDisplayed()
        {
            isDisplayed(By.XPath("//section[contains(@class,'os-section--confirmation')]"), 10, "Order success page isn't displayed");
        }
    }
}
