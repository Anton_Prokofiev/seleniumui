﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWAdditionalServiceSummaryPage : MainPageOnlineSales
    {
        private Lazy<IWebElement> LicensePlateChkBox;
        private Lazy<IWebElement> DatePickerContainer;
        private Lazy<IWebElement> DatePickerList;
        private Lazy<IWebElement> AddressSummarybtn;
        private Lazy<IList<IWebElement>> DeliveryAppointmentDateList;
        private Lazy<IWebElement> Submit;


        public PKWAdditionalServiceSummaryPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver)
        {
            AddressSummarybtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("os-address-summary__button"));
            LicensePlateChkBox = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-desired-license-plate__checkbox-container .os-icon--small"));
            DatePickerContainer = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".react-datepicker__input-container"));
            DeliveryAppointmentDateList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector(".os-delivery-appointment__daytime-list .os-checkbox"));
            Submit = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("os-button os-button--primary"));
        }
        public void ClickOnAuslieferungDeliveryMethod()
        {
            clickOnElement(By.XPath("//button[@id='service-selection-delivery-c2a']"), 10, "can't click on adress delivery method");
        }
        public void ClickOnAbholungDeliveryMethod()
        {
            clickOnElement(By.XPath("//button[@id='service-selection-pick-up-c2a']"), 10, "can't click on adress delivery method");
        }

        public void ClickOnPickUpMethod()
        {
            clickOnElement(By.XPath("//button[@id='service-selection-pick-up-c2a']"), 10, "can't click on pick up method");
        }

        public void FillLicensePlateFields()
        {
            clearAndFillField(By.XPath("//input[@id='districtCode']"),"AQA", 10, "can't fill the districtCode part of licence plate");

            clearAndFillField(By.XPath("//input[@id='letters']"), "QA", 10, "can't fill the first letters part of licence plate");

            clearAndFillField(By.XPath("//input[@id='numbers']"), "0000", 10, "can't fill the first numbers part of licence plate");

        }
            public void ChooseTheDateFromCelendarAbholung()
        {
            clickOnElement(By.XPath("//input[@id='Input--pickupDateOne']"), 5, "Can't click on celendar");

            String datePickerLocator = "(//div[contains(@class,'react-datepicker__day react-datepicker__day') and not(contains(@class,'disabled'))])";

            int upperBound = getElementsCount(By.XPath(datePickerLocator), "can't get count of days in celendar");
            int rnd = randomNumber(1, upperBound);
            clickOnElement(By.XPath(datePickerLocator + "[" + rnd + "]"), 5, "can't click on day number" + rnd);

        }
        public void ChooseTheDateFromCelendar()
        {
            clickOnElement(By.XPath("//input[@id='Input--deliveryDateOne']"), 5, "Can't click on celendar");

            String datePickerLocator = "(//div[contains(@class,'react-datepicker__day react-datepicker__day') and not(contains(@class,'disabled'))])";

            int upperBound = getElementsCount(By.XPath(datePickerLocator), "can't get count of days in celendar");
            int rnd = randomNumber(1, upperBound);
            clickOnElement(By.XPath(datePickerLocator+ "["+ rnd +"]"), 5, "can't click on day number" + rnd);
            
        }
        public void ChooseDeliveryTime()
        {
            clickOnElement(By.XPath("//input[@id='Input--pickupTimeOne']/parent::div"), 5, "can't click on deliver time field");
            clickOnElement(By.XPath("//div[@id='pickupTimeOne-picker']//li[1]"), 5, "can't click on deliveryDateOneMorning Chkbox");

            
        }
        public void ClickOndeliveryDateOneMorningChkbox()
        {
            clickOnElement(By.XPath("//label[@for='deliveryDateOneMorning']"), 5, "can't click on deliveryDateOneMorning Chkbox");
        }
        public LightBox OpenBillindAddressModal()
        {
            clickOnElement(By.XPath("//button[@class='os-address-summary__button']"), 5, "can't click on open biling adress modal");
            
            return driver.CreateLightBox<LightBox>();
        }
        public void ClickOnSubmitBtn()
        {
            clickOnElement(By.XPath("//button[@id='additional-services-summary-next-c2a']"), 5, "Can't click on submit button 'Weiter zur Bezahlung'");
        }
        
    }
}
