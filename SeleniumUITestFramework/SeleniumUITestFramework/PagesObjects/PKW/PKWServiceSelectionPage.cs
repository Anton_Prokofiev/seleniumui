﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWServiceSelectionPage : IPage
    {
        private Lazy<IWebElement> DeliveryBtn;
        private Lazy<IWebElement> PickUpBox;

        public void Init(RemoteWebDriver driver)
        {
            DeliveryBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#service-selection-delivery-c2a"));
            PickUpBox  = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#service-selection-pick-up-c2a"));
        }
    }
}
