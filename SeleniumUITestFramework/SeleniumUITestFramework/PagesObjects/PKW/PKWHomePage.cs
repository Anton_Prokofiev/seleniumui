﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWHomePage : MainPageOnlineSales
    {
        private Lazy<IWebElement> OrderOnlineNowBtn;

        public PKWHomePage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        { 
            OrderOnlineNowBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-stage .os-button"));
        }

        public void Open(RemoteWebDriver driver)
        {
        }
        public WaitHelper Wait()
        {
            return new WaitHelper();
        }

        public bool UntilPageLoaded => OrderOnlineNowBtn.Value.Displayed;
        public LightBox OrderOnlineNowBtnClick(RemoteWebDriver driver)
        {
            OrderOnlineNowBtn.Value.Click();
            return driver.CreateLightBox<LightBox>();
        }
    }
}

