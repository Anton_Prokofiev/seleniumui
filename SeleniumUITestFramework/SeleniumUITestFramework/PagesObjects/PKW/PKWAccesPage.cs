﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;


namespace SeleniumUITestFramework.PagesObjects
{
    class PKWAccesPage : MainPageOnlineSales
    {
        private String URL = ConfigurationSettings.AppSettings["PKWHomePageURL"];
        private IWebElement CarKeyTestField;

        public PKWAccesPage(RemoteWebDriver driver) : base(driver)
        {
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {
        }
        public void Open(RemoteWebDriver driver, string carKey)
        {
            String AccessPageURL = ConfigurationSettings.AppSettings["PKWAccessPageURL"];


            driver.Navigate().GoToUrl(AccessPageURL);
            driver.Manage().Window.Maximize();

            
            driver.Navigate().GoToUrl(URL + carKey);
        }
        public String getUrl()
        {
            return this.URL;
        }

        public void SubmitCarKey(RemoteWebDriver driver)
        {          
           CarKeyTestField.SendKeys(ConfigurationSettings.AppSettings["CarKey"]);
        }
    }
}
