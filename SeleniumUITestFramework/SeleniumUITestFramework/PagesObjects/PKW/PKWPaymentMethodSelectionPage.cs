﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWPaymentMethodSelectionPage : MainPageOnlineSales
    {
        /*
        private Lazy<IWebElement> DirectPaymentBtn;
        private Lazy<IWebElement> PrivacyPolicyChkBox;
        private Lazy<IWebElement> PrivacyPolicyErrorMsg;
        */
        private By DirectPaymentBtnLocator = By.XPath("//button[@id='payment-method-direct-payment-c2a']");
        private By PrivacyPolicyErrorMsgLocator = By.XPath("//div[@class='os-checkbox__error']");
        private By PrivacyPolicyChkBoxLocator = By.XPath("//label[@for='agb']//span[contains(@class,'os-checkbox__icon')]");
        private By EmailChkBoxLocator = By.XPath("//label[@for='email-checkbox']//span[contains(@class,'os-checkbox__icon')]");
        private By EmailInputFieldLocator = By.XPath("//input[@id='Input--email']");


        public PKWPaymentMethodSelectionPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver)
        {
            /*
            Lazy<IWebElement> DirectPaymentBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#payment-method-direct-payment-c2a .os-button__caption"));
            Lazy<IWebElement> PrivacyPolicyChkBox = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("label[for='agb'] .os-icon--small"));
            PrivacyPolicyErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("label[for= 'agb'] .os-checkbox__error"));
            */
        }
        

        public void ClickOnMailChkBox()
        {
            lazyClickOnElement(EmailChkBoxLocator, 5, "Can't click on email checkbox");
        }
        
        public void ClickOnPrivacyPolicyChkBox()
        {
            clickOnElement(PrivacyPolicyChkBoxLocator, 10, "can't click on PrivacyPolicyChkBox");
        }
        public void ClickOnDirectPaymentBtn()
        {
            clickOnElement(DirectPaymentBtnLocator, 10, "Can't click on Direct payment button");
        }

        public void IsPrivacyPolicyErrMsgIsDisplayed()
        {
            isDisplayed(PrivacyPolicyErrorMsgLocator, 5, "PrivacyPolicyErrorMsg isn't displayed");
        }
        public void IsEmailInputFieldEqualVWID()
        {
            Console.WriteLine("");
            Lazy<IWebElement> lazyInput = new Lazy<IWebElement>(() => driver.FindElement(By.XPath("//div[@class='os-input']")));
            lazyInput.Value.Click();
            //String str = getElement(EmailInputFieldLocator, "can't get emailInputField").Text;
            String email = getTextFromElement(EmailInputFieldLocator, 5, "Can't get the text from email input field");

            Boolean isEmailEqual = email.Equals(ConfigurationSettings.AppSettings["Login"]);
            Assert.IsTrue(isEmailEqual, "emails are not equal");
        }
    }
}
