﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;


namespace SeleniumUITestFramework.PagesObjects.PKW
{
    class PKWPaymentPage : MainPageOnlineSales
    {
        public PKWPaymentPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver)
        {
            //throw new NotImplementedException();
        }
        public void ChoosePaymentMethodByBankTransfer()
        {
            clickOnElement(By.XPath("//button[@id='payment-type-transfer-c2a']"), 10, "Can't click on payment method");
            clickOnElement(By.XPath("//label[@for='agb']//span[contains(@class,'os-checkbox__icon')]"), 5, "Can't click on privacy checkbox");
            clickOnElement(By.XPath("//button[@id='purchase-c2a']"), 5, "Can't click on purchase button");

        }
    }
}
