﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.BreadCrumbs;

namespace SeleniumUITestFramework.PagesObjects
{
    class NFZContactDataPage : MainPageOnlineSales
    {
        private Lazy<IList<IWebElement>> TitleDropDownList;
        private Lazy<IList<IWebElement>> SalutationDropDownList;
        private Lazy<IWebElement> TitleDropDownbtn;
        private Lazy<IWebElement> SalutationDropDownbtn;
        private Lazy<IWebElement> FirstNameTextField;
        private Lazy<IWebElement> FirstnameFiedlCloseBtn;
        private Lazy<IWebElement> SecondNameTextField;
        private Lazy<IWebElement> EmailCheckBox;
        private Lazy<IWebElement> TelephoneCheckBox;
        public Lazy<IWebElement> EmailField;
        private Lazy<IWebElement> TelephoneNumberField;
        private Lazy<IWebElement> DataUseAgreementOptionalCheckBox;
        private Lazy<IWebElement> DataUseAgreementCheckBox;
        private Lazy<IWebElement> SubmitBtn;
        public IWebElement FirstNameErrorMsg;
        public IWebElement LastNameErrorMsg;
        public IWebElement EmailErrorMsg;
        public IWebElement TelephoneErrorMsg;

        public NFZContactDataPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {
            TitleDropDownbtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#title"));
            TitleDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector("#title > option"));
            SalutationDropDownbtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#salutation"));
            SalutationDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector("#salutation > option"));
            FirstNameTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--firstName"));
            SecondNameTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--lastName"));
            EmailCheckBox = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#contactByEmail"));
            TelephoneCheckBox = new Lazy<IWebElement>(() => driver.FindElementById("contactByPhone"));
            EmailField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--email"));
//            TelephoneNumberField = null;
            DataUseAgreementOptionalCheckBox = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("label[for=\'dataUseAgreementOptional\'] > span > span.os-checkbox__icon.os-icon.os-icon--small"));
            DataUseAgreementCheckBox = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("label[for=\'dataUseAgreement\'] > span > span.os-checkbox__icon.os-icon.os-icon--small"));
            SubmitBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-button__caption"));
        }

        public void Open(RemoteWebDriver driver)
        {
        }
        public WaitHelper Wait()
        {
            return new WaitHelper();
        }

        public void FillInContactDataForm()
        {
            FirstNameTextField.Value.SendKeys(ConfigurationSettings.AppSettings["FirstName"]);
            SecondNameTextField.Value.SendKeys(ConfigurationSettings.AppSettings["SecondName"]);
            EmailField.Value.SendKeys(ConfigurationSettings.AppSettings["Login"]);           
 //           DataUseAgreementOptionalCheckBox.Value.Click();
 //           DataUseAgreementCheckBox.Value.Click();
            SubmitBtn.Value.Click();
        }

        public void FillInContactDataFormWithParam(string firstName, string secondName, string userEmail)
        {
            FirstNameTextField.Value.SendKeys(firstName);
            SecondNameTextField.Value.SendKeys(secondName);
            Thread.Sleep(1000);
            EmailField.Value.SendKeys(userEmail);
            SubmitBtn.Value.Click();
        }

        public void FillInContactFormWithPhone(RemoteWebDriver driver)
        {
            SelectPhoneCheckBox(driver);
            TelephoneNumberField.Value.SendKeys(ConfigurationSettings.AppSettings["PhoneNumber"]);
            FillInContactDataForm();
        }

        public void SelectPhoneCheckBox(RemoteWebDriver driver)
        {
            TelephoneNumberField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--mobile"));
            if (!TelephoneCheckBox.Value.Selected)
            {
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                js.ExecuteScript("document.getElementById('contactByPhone').click();");
            }              
        }

        public void ClearContactFormWithPhoneField(RemoteWebDriver driver)
        {
            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(Keys.Control + "a");
            FirstNameTextField.Value.SendKeys(Keys.Delete);
            SecondNameTextField.Value.Click();
            Thread.Sleep(300);
            FirstNameErrorMsg = driver.FindElementByCssSelector("[data-scroll-container='firstName'] .os-input__error");
            SecondNameTextField.Value.SendKeys(Keys.Control + "a");
            SecondNameTextField.Value.SendKeys(Keys.Delete);
            EmailField.Value.Click();
            Thread.Sleep(300);
            LastNameErrorMsg = driver.FindElementByCssSelector("[data-scroll-container='lastName'] .os-input__error");
            EmailField.Value.SendKeys(Keys.Control + "a");
            EmailField.Value.SendKeys(Keys.Delete);
            var t= driver.FindElementById("contactByPhone");
            if (t.Selected)
            {              
                TelephoneNumberField.Value.Click();
                Thread.Sleep(300);
                EmailErrorMsg = driver.FindElementByCssSelector("[data-scroll-container='userEmail'] .os-input__error");
                TelephoneNumberField.Value.SendKeys(Keys.Control + "a");
                TelephoneNumberField.Value.SendKeys(Keys.Delete);
            }
            SecondNameTextField.Value.Click();
            Thread.Sleep(300);
            TelephoneErrorMsg = driver.FindElementByCssSelector("[data-scroll-container='telephone'] .os-input__error");
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0, 600)");
        }

        public void ClearContactFormWithoutPhoneField(RemoteWebDriver driver)
        {
            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(Keys.Control + "a");
            FirstNameTextField.Value.SendKeys(Keys.Delete);
            SecondNameTextField.Value.Click();
            Thread.Sleep(300);
            SecondNameTextField.Value.SendKeys(Keys.Control + "a");
            SecondNameTextField.Value.SendKeys(Keys.Delete);
            EmailField.Value.Click();
            Thread.Sleep(300);
            EmailField.Value.SendKeys(Keys.Control + "a");
            EmailField.Value.SendKeys(Keys.Delete);
        }

        public void SelectValuesFromDropDownMenu(RemoteWebDriver driver, string title, string salutation)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0,-900)");
            TitleDropDownbtn.Value.Click();
            TitleDropDownList.Value.First(x => x.Text.Contains(title)).Click();
            SalutationDropDownbtn.Value.Click();
            SalutationDropDownList.Value.First(x => x.Text.Contains(salutation)).Click();
        }

        public bool IsFirstNameErrorMsgDisplayed => FirstNameErrorMsg.Displayed;
        public bool IsLastNameErrorMsgDisplayed => LastNameErrorMsg.Displayed;
        public bool IsEmailErrorMsgDisplayed => EmailErrorMsg.Displayed;
        public bool IsTelephoneErrorMsgDisplayed => TelephoneErrorMsg.Displayed;
        public string FirstNameValue => FirstNameTextField.Value.GetAttribute("value");
        public string SecondNameValue => SecondNameTextField.Value.GetAttribute("value");
        public string EmailFieldValue => EmailField.Value.GetAttribute("value");
    }
}
