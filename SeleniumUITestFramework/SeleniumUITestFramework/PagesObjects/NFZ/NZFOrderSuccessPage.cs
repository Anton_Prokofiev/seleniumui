﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;



namespace SeleniumUITestFramework.PagesObjects.NFZ
{
    class NZFOrderSuccessPage : MainPageOnlineSales
    {
        public NZFOrderSuccessPage(RemoteWebDriver driver) : base(driver)
        {
        }
        public void IsOrderSuccessPageDisplayed()
        {
            isDisplayed(By.XPath("//div[contains(@class,'os-congratulations')]"), 10, "Order success page isn't displayed");
        }
    }
}
