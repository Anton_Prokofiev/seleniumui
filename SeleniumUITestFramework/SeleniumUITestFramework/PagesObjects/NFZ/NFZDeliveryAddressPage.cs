﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;
using OpenQA.Selenium.Support.UI;

namespace SeleniumUITestFramework.PagesObjects
{
    class NFZDeliveryAddressPage : MainPageOnlineSales
    {
        private Lazy<IWebElement> SubmitBtn;
        private Lazy<IWebElement> FirstNameTextField;
        public Lazy<IWebElement> FirstNameErrorMsg;
        public Lazy<IWebElement> LastNameErrorMsg;
        public Lazy<IWebElement> StreetErrorMsg;
        public Lazy<IWebElement> HouseNumberErrorMsg;
        public Lazy<IWebElement> PostalCodeErrorMsg;
        public Lazy<IWebElement> CityErrorMsg;
        private Lazy<IWebElement> SecondNameTextField;
        private Lazy<IWebElement> TitleDropDown;
        private Lazy<IWebElement> SalutationDropDown;
        private Lazy<IWebElement> StreetTextField;
        private Lazy<IWebElement> HouseNumberTextField;
        private Lazy<IWebElement> CompanyTextField;
        private Lazy<IWebElement> ZipCodeTextField;
        private Lazy<IWebElement> CityTextField;
        private Lazy<IWebElement> BillingAddressLink;

        By StreetTextFieldLocator = By.CssSelector("#Input--street");
        By HouseNumberTextFieldLocator = By.CssSelector("#Input--houseNumber");
        By CompanyTextFieldLocator = By.CssSelector("#Input--company");
        By ZipCodeTextFieldLocator = By.CssSelector("#Input--zipCode");
        By CityTextFieldLocator = By.CssSelector("#Input--city");

        By CityErrorMsgLocator = By.CssSelector("[data-scroll-container='city'] .os-input__error");

        public NFZDeliveryAddressPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {

            SubmitBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-form__form .os-button--primary"));
            FirstNameTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--firstName"));
            SecondNameTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--lastName"));
            TitleDropDown = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-form__form [name='title']"));
            SalutationDropDown = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-form__form [data-scroll-container='salutation'] .os-select__caption"));

            StreetTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--street"));
            HouseNumberTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--houseNumber"));
            CompanyTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--company"));
            ZipCodeTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--zipCode"));
            CityTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#Input--city"));
            BillingAddressLink = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-form__form .os-navigation-link__caption"));
            FirstNameErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='firstName'] .os-input__error"));
            LastNameErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='lastName'] .os-input__error"));
            CityErrorMsg = new Lazy<IWebElement>(() => driver.FindElement(CityErrorMsgLocator));
            StreetErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='street'] .os-input__error"));
            HouseNumberErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='houseNumber'] .os-input__error"));
            PostalCodeErrorMsg = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("[data-scroll-container='zipCode'] .os-input__error"));


            
        }

        public void Open(RemoteWebDriver driver)
        {
        }
        public WaitHelper Wait()
        {
            return new WaitHelper();
        }

        public void ClearDeliveryForm()
        {
            clickOnElement(By.CssSelector("#Input--firstName"), 5, "can't click on FIRST_NAME field");

            //FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(Keys.Control + "a");
            FirstNameTextField.Value.SendKeys(Keys.Delete);
            SecondNameTextField.Value.Click();
            SecondNameTextField.Value.SendKeys(Keys.Control + "a");
            SecondNameTextField.Value.SendKeys(Keys.Delete);
            
            clickOnElement(StreetTextFieldLocator, 10, "can't click on Street text field");


            
            

            StreetTextField.Value.Click();
            StreetTextField.Value.SendKeys(Keys.Control + "a");
            StreetTextField.Value.SendKeys(Keys.Delete);
            HouseNumberTextField.Value.Click();
            HouseNumberTextField.Value.SendKeys(Keys.Control + "a");
            HouseNumberTextField.Value.SendKeys(Keys.Delete);
            CompanyTextField.Value.Click();
            CompanyTextField.Value.SendKeys(Keys.Control + "a");
            CompanyTextField.Value.SendKeys(Keys.Delete);
            ZipCodeTextField.Value.Click();
            ZipCodeTextField.Value.SendKeys(Keys.Control + "a");
            ZipCodeTextField.Value.SendKeys(Keys.Delete);
            CityTextField.Value.Click();
            CityTextField.Value.SendKeys(Keys.Control + "a");
            CityTextField.Value.SendKeys(Keys.Delete);
            

            FirstNameTextField.Value.Click();
            isDisplayed(CityErrorMsgLocator, 5, "City error message isn't displayed");
            

        }

        public void FillInDeliveryForm(RemoteWebDriver driver)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0,-900)");
            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(ConfigurationSettings.AppSettings["FirstName"]);
            SecondNameTextField.Value.Click();
            SecondNameTextField.Value.SendKeys(ConfigurationSettings.AppSettings["SecondName"]);

            
            clickOnElement(StreetTextFieldLocator, 5, "can't click on street field");
            clearAndFillField(StreetTextFieldLocator, ConfigurationSettings.AppSettings["Street"], 5, "can't fill the street field");

            
            clickOnElement(HouseNumberTextFieldLocator, 5, "can't click on street field");
            clearAndFillField(HouseNumberTextFieldLocator, ConfigurationSettings.AppSettings["HouseNumber"], 5, "can't fill the HouseNumber field");

            
            clickOnElement(CompanyTextFieldLocator, 5, "can't click on street field");
            clearAndFillField(CompanyTextFieldLocator, ConfigurationSettings.AppSettings["Company"], 5, "can't fill the Company field");

            
            clickOnElement(ZipCodeTextFieldLocator, 5, "can't click on street field");
            clearAndFillField(ZipCodeTextFieldLocator, ConfigurationSettings.AppSettings["ZipCode"], 5, "can't fill the ZipCode field");

            
            clickOnElement(CityTextFieldLocator, 5, "can't click on street field");
            clearAndFillField(CityTextFieldLocator, ConfigurationSettings.AppSettings["City"], 5, "can't fill the City field");


        }

        public void SubmitForm()
        {
            clickOnElement(By.CssSelector("#delivery-address-form-c2a-submit"), 5, "can't click on submit button");

            //SubmitBtn.Value.Click();
        }
        public void FillInDeliveryAdressForm()
        {
            By StreetTextFieldLocator = By.CssSelector("#Input--street");
            clickOnElement(StreetTextFieldLocator, 5, "can't click on street field");
            clearAndFillField(StreetTextFieldLocator, ConfigurationSettings.AppSettings["Street"], 5, "can't fill the street field");

            By HouseNumberTextField = By.CssSelector("#Input--houseNumber");
            clickOnElement(HouseNumberTextField, 5, "can't click on street field");
            clearAndFillField(HouseNumberTextField, ConfigurationSettings.AppSettings["HouseNumber"], 5, "can't fill the HouseNumber field");

            By CompanyTextField = By.CssSelector("#Input--company");
            clickOnElement(CompanyTextField, 5, "can't click on street field");
            clearAndFillField(CompanyTextField, ConfigurationSettings.AppSettings["Company"], 5, "can't fill the Company field");

            By ZipCodeTextField = By.CssSelector("#Input--zipCode");
            clickOnElement(ZipCodeTextField, 5, "can't click on street field");
            clearAndFillField(ZipCodeTextField, ConfigurationSettings.AppSettings["ZipCode"], 5, "can't fill the ZipCode field");

            By CityTextField = By.CssSelector("#Input--city");
            clickOnElement(CityTextField, 5, "can't click on street field");
            clearAndFillField(CityTextField, ConfigurationSettings.AppSettings["City"], 5, "can't fill the City field");



        }

        public string FirstNameValue => FirstNameTextField.Value.GetAttribute("value");
        public string SecondNameValue => SecondNameTextField.Value.GetAttribute("value");

        public string TitleValue(RemoteWebDriver driver)
        {
            By selectLocator = By.CssSelector(".os-form__form [name='title']");
//            isDisplayed(selectLocator, 5, "Select field TITLE isn't displayed");
            SelectElement select = new SelectElement(getElement(selectLocator, "can't get the select field TITLE"));
            //select[@name='title']
            //var s = new SelectElement(driver.FindElementByCssSelector(".os-form__form [name='title']"));
            return select.SelectedOption.Text;
        }
        public string SalutationValue()
        {
            return SalutationDropDown.Value.Text;
        }

        public LightBox OpenBillindAddressModal(RemoteWebDriver driver)
        {
            BillingAddressLink.Value.Click();
            return driver.CreateLightBox<LightBox>();
        }
        public bool IsFirstNameErrorMsgDisplayed => FirstNameErrorMsg.Value.Displayed;
        public bool IsLastNameErrorMsgDisplayed => LastNameErrorMsg.Value.Displayed;
        public bool IsCityErrorMsgDisplayed => CityErrorMsg.Value.Displayed;
        public bool IsPostalCodeErrorMsgDisplayed => PostalCodeErrorMsg.Value.Displayed;
        public bool IsStreetErrorMsgDisplayed => StreetErrorMsg.Value.Displayed;
        public bool IsHouseNumberErrorMsgDisplayed => HouseNumberErrorMsg.Value.Displayed;
    }
}
