﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;

namespace SeleniumUITestFramework.PagesObjects
{
    class NFZHomePage : MainPageOnlineSales
    {
        private Lazy<IWebElement> OrderOnlineNowBtn;
        private Lazy<IWebElement> OnlinePurchaseBtn;
        private Lazy<IWebElement> GewerbeBtn;
        private Lazy<IWebElement> DirectPaymentBtn;
        private Lazy<IWebElement> DigitalSellerLink;
        private Lazy<IWebElement> CookieNotificationCloseBtn;

        public NFZHomePage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {
            OrderOnlineNowBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-stage .os-button"));
            OnlinePurchaseBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-button--primary"));
            GewerbeBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #COMMERCIAL"));
            DirectPaymentBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #DIRECT_PAYMENT"));
            DigitalSellerLink = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-app-header-nfz__action-list__item--contact"));
            CookieNotificationCloseBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".vw_m641_system_notification_close"));
        }

        public void Open(RemoteWebDriver driver)
        {
        }

        public bool IsPageLoaded => OrderOnlineNowBtn.Value.Displayed;
        public void CookieNotificationClose()
        {
            CookieNotificationCloseBtn.Value.Click();
        }

        public WaitHelper Wait()
        {
            return new WaitHelper();
        }
        public LightBox OpenDigitalSellerLightBox(RemoteWebDriver driver)
        {
            DigitalSellerLink.Value.Click();
            return driver.CreateLightBox<LightBox>();
        }
        public LightBox OrderOnlineNowBtnClick(RemoteWebDriver driver)
        {
            lazyClickOnElement(By.XPath("//button[@id='landing-page-next-c2a']"), 5, "can't click on 'Fahrzeug jetzt online bestellen' button");
            //OrderOnlineNowBtn.Value.Click();
            return driver.CreateLightBox<LightBox>();
        }
        

        public void OnlinePurchaseBtnClick(RemoteWebDriver driver)
        {
            
           
            OnlinePurchaseBtn.Value.Click();
        }

        public void GewerbeTileClick()
        {
            GewerbeBtn.Value.Click();
        }
        public void DirectPaymentTileClick()
        {
            //driver.FindElementByXPath("//label[@id='DIRECT_PAYMENT']");
            lazyClickOnElement(By.XPath("//label[@id='DIRECT_PAYMENT']"), 5, "can't click on 'Direktbezahlung' button");
            //DirectPaymentBtn.Value.Click();
        }
    }
}
