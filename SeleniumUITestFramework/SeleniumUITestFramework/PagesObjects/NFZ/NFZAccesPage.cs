﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects
{
    class NFZAccesPage : MainPageOnlineSales
    {
        private IWebElement CarKeyTestField;

        public NFZAccesPage(RemoteWebDriver driver) : base(driver)
        {
        }

        public void Open(string carKey)
        {
            var URL = ConfigurationSettings.AppSettings["NFZAccessPageURL"];
            driver.Navigate().GoToUrl(URL);
            driver.Manage().Window.Maximize();

            var URLHomePage = ConfigurationSettings.AppSettings["NFZHomePageURL"];
            driver.Navigate().GoToUrl(URLHomePage + carKey);
        }

        public void SubmitCarKey()
        {          
           CarKeyTestField.SendKeys(ConfigurationSettings.AppSettings["CarKey"]);
        }
    }
}
