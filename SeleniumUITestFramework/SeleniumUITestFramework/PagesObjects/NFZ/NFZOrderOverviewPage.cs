﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects
{
    class NFZOrderOverviewPage : MainPageOnlineSales
    {
        private Lazy<IWebElement> DeliveryRemark;
        private Lazy<IWebElement> DataProtectionPolicy;
        private Lazy<IWebElement> SubmitBtn;
        private Lazy<IList<IWebElement>> ContactDataTable;
        private Lazy<IList<IWebElement>> PaymentDataTable;
        private Lazy<IList<IWebElement>> DeliveryTable;
        private Lazy<IList<IWebElement>> BillingTable;

        public NFZOrderOverviewPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver) // Init Web elements due to obsoleted PageFactory
        {
            DeliveryRemark = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("label[for='agb'] .os-icon--small"));
            DataProtectionPolicy = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("label[for='dataProtectionPolicy'] .os-icon--small"));
            SubmitBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-button__caption"));
            ContactDataTable = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector(".os-contact-data-section__table"));
            PaymentDataTable = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector(".os-payment-type-section__table"));
            DeliveryTable = new Lazy<IList<IWebElement>>(() => driver.FindElementsByClassName("os-addresses__delivery-address"));
            BillingTable = new Lazy<IList<IWebElement>>(() => driver.FindElementsByClassName("os-addresses__billing-address"));
        }

        public void Open(RemoteWebDriver driver)
        {
        }
        public WaitHelper Wait()
        {
            return new WaitHelper();
        }

        public string RetrieveContactDataValues(int value)
        {
           IList<IWebElement> tabledata = ContactDataTable.Value.First().FindElements(By.TagName("td"));
           string s = tabledata[value].Text;
           return s;
        }
        public string RetrievePaymentDataValues(int value)
        {
            IList<IWebElement> tabledata = PaymentDataTable.Value.First().FindElements(By.TagName("td"));
            string s = tabledata[value].Text;
            return s;
        }

        public string RetrieveDeliveryAddressData(int value)
        {
            IList<IWebElement> tabledata = DeliveryTable.Value.First().FindElements(By.TagName("p"));
            string s = tabledata[value].Text;
            return s;
        }

        public string RetrieveBillingAddressData(int value)
        {
            IList<IWebElement> tabledata = BillingTable.Value.First().FindElements(By.TagName("p"));
            string s = tabledata[value].Text;
            return s;
        }

        public void SubmitDeliveryRemarks()
        {
            clickOnElement(By.CssSelector("label[for='agb'] .os-icon--small"),5, "can't click on DeliveryRemark");
            //DeliveryRemark.Value.Click();
            clickOnElement(By.CssSelector("label[for='dataProtectionPolicy'] .os-icon--small"), 5, "can't click on DataProtectionPolicy");
            //DataProtectionPolicy.Value.Click();
            clickOnElement(By.CssSelector(".os-button__caption"), 5, "can't click on submit button");
            //SubmitBtn.Value.Click();
        }
    }
}
