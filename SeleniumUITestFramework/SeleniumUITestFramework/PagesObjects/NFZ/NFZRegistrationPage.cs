﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;

namespace SeleniumUITestFramework.PagesObjects.NFZ
{
    class NFZRegistrationPage : MainPageOnlineSales

    {
        private Lazy<IWebElement> ContinueOnlineOrderBtn;

        public NFZRegistrationPage(RemoteWebDriver driver) : base(driver)
        {
            Init(driver);
        }

        public void Init(RemoteWebDriver driver)
        {
            ContinueOnlineOrderBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-registration-view__button.os-button.os-button--primary"));
        }
        public WaitHelper Wait()
        {
            return new WaitHelper();
        }
        public void ContinueOnlineOrderClick()
        {
            lazyClickOnElement(By.XPath("//button[contains(@class ,'os-registration-view__button')]"), 10, "can't click on 'Weiter mit der Online-Bestellung' button");
            //ContinueOnlineOrderBtn.Value.Click();   
        }
    }
}
