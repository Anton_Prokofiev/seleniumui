﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.PagesObjects
{
    class StatePage : MainPageOnlineSales
    {
        
        public StatePage(RemoteWebDriver driver) : base(driver)
        {
        }
        public void UnblockCurrentCar(String siteUrl)
        {

            String previousURL = driver.Url;
            driver.Navigate().GoToUrl(siteUrl + "state");
            //Thread.Sleep(2000);
            try
            {
                clickOnElement(By.XPath("//span[contains(text(),'Unblock car')]"), 10, "can't click on unblock car");
            }
            catch (WebDriverException e)
            {
                Console.WriteLine(e.Message);
            }
            driver.Navigate().Refresh();
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl(previousURL);
        }
    }
}
