﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using RestSharp;
using RestSharp.Authenticators;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;

namespace SeleniumUITestFramework.Tests.API_Tests
{
    [TestFixture]
    [Category("API")]
    class APIDeleteUserDataTest : MainTestOnlineSales
    {
        [Test(Description =
            "API delete User Data tests")]

        public void APIDELETEUserDataTest()
        {
            var driver = DriverSetUp("Chrome")
                .InitPage(new NFZAccesPage(GetDriver()), accesspage =>
                {
                    accesspage.Open("?key=REVVMDE5MzcyMzYwNTk%3D");
                    //?key=REVVNDUwODBWRFYyMzQ%3D

                })
                .InitPage(new NFZHomePage(GetDriver()), homepage =>
                {
                    Thread.Sleep(2000);
                    homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
                    homepage.GewerbeTileClick();
                    homepage.OnlinePurchaseBtnClick(GetDriver());
                })
                .InitPage(new NFZVolkswagenLoginPage(GetDriver()), loginpage =>
                {
                    loginpage.LoginWithParameters("antonprok2+35@gmail.com", "Cawu5122");                  
                });
            var accessToken = driver.Manage().Cookies.GetCookieNamed("access-token").Value; 

            var client = new RestClient("https://nfz-vwos-dev.extern.etecture.de");
            client.Authenticator = new JwtAuthenticator(accessToken);

            var request = new RestRequest("api-dev/nfz/userdata", Method.DELETE);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Basic d2F0b3N1c2VyOlNVMzkycGVy");
            request.AddHeader("X-Authorization", "Bearer " + accessToken);

            var response = client.Execute(request).StatusCode;
            int numericStatusCode = (int)response;
            Console.WriteLine(numericStatusCode);

            //driver.Quit();       
        }
    }
}
