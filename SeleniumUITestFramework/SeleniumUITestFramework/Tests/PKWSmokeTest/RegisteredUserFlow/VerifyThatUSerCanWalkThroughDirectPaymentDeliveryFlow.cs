﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using SeleniumUITestFramework.PagesObjects.PKW;

namespace SeleniumUITestFramework.Tests.PKWSmokeTest.RegisteredUserFlow
{
    class VerifyThatUSerCanWalkThroughDirectPaymentDeliveryFlow
    {
        [Category("Smoke PKW")]
        //[Ignore("In progress of development")]
        public class VerifyPKWPrivatDirektbezahlungFlow : MainTestOnlineSales
        {

            [Test(Description =
                "Verify that user can successfully walk-through Direct payment 'Auslieferung' delivery flow (already registered user)")]
            [TestCase("Chrome")]
            public void VerifyThatUserCanWalkThroughDirectPaymentAuslieferungDeliveryFlow(string BrowserName)
            {
                String siteUrl = "";

                DriverSetUp(BrowserName)
                .InitPage(new PKWAccesPage(GetDriver()), accesspage =>
                {
                    accesspage.Open(GetDriver(), "?key=REVVNDUwODBWRFYyMzQ%3D");
                    siteUrl = accesspage.getUrl();
                })
                .InitPage(new PKWHomePage(GetDriver()), homepage =>
                {

                    homepage.Wait().WaitForReady(GetDriver());
                    homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
                }).InitPage(new PKWVolkswagenLoginPage(GetDriver()), loginpage =>
                {
                    loginpage.Login();
                }).InitPage(new StatePage(GetDriver()), statePage =>
                {
                    statePage.UnblockCurrentCar(siteUrl); // TODO rewrite it to api call
                })
                .InitPage(new PKWPaymentMethodSelectionPage(GetDriver()), paymentSelectionPage =>
                {
                    paymentSelectionPage.ClickOnDirectPaymentBtn();
                    paymentSelectionPage.IsPrivacyPolicyErrMsgIsDisplayed();

                    Thread.Sleep(200);
                    paymentSelectionPage.ClickOnMailChkBox();



                    paymentSelectionPage.ClickOnPrivacyPolicyChkBox();

                    //paymentSelectionPage.IsEmailInputFieldEqualVWID(); // TODO can't get the text from input email input field



                    paymentSelectionPage.ClickOnDirectPaymentBtn();


                }).InitPage(new PKWAdditionalServiceSummaryPage(GetDriver()), additionalService =>
                {
                    additionalService.ClickOnAuslieferungDeliveryMethod();
                    additionalService.FillLicensePlateFields();
                    additionalService.ChooseTheDateFromCelendar();
                    Thread.Sleep(200);
                    additionalService.ClickOndeliveryDateOneMorningChkbox();
                    additionalService.OpenBillindAddressModal().ClearBillingForm(GetDriver()).PKWFillInDeliveryForm(GetDriver());
                    additionalService.ClickOnSubmitBtn();

                }).InitPage(new PKWPaymentPage(GetDriver()), paymentPage =>
                {
                    paymentPage.ChoosePaymentMethodByBankTransfer();
                }).InitPage(new PKWOrderSuccessPage(GetDriver()), orderSuccessPage =>
                {
                    orderSuccessPage.IsOrderSuccessPageDisplayed();
                });
            }
            [Test(Description =
                "Verify that user can successfully walk-through Direct payment 'Abholung' delivery flow (already registered user)")]
            [TestCase("Chrome")]
            //[Ignore("In progress of development")]
            public void VerifyThatUserCanWalkThroughDirectPaymentAbholungDeliveryFlow(string BrowserName)
            {
                String siteUrl = "";
                //https://vwos-dev.extern.etecture.de/?key=REVVNDUwODBWRFYyMzQ%3D
                DriverSetUp(BrowserName)
                .InitPage(new PKWAccesPage(GetDriver()), accesspage =>
                {
                    siteUrl = accesspage.getUrl();
                    accesspage.Open(GetDriver(), "?key=REVVNDUwODBWRFYyMzQ%3D");
                    
                })
                .InitPage(new PKWHomePage(GetDriver()), homepage =>
                {

                    homepage.Wait().WaitForReady(GetDriver());
                    homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
                }).InitPage(new PKWVolkswagenLoginPage(GetDriver()), loginpage =>
                {
                    loginpage.Login();
                }).InitPage(new StatePage(GetDriver()), statePage =>
                {
                    statePage.UnblockCurrentCar(siteUrl); // TODO rewrite it to api call
                })
                .InitPage(new PKWPaymentMethodSelectionPage(GetDriver()), paymentSelectionPage =>
                {
                    paymentSelectionPage.ClickOnDirectPaymentBtn();
                    paymentSelectionPage.IsPrivacyPolicyErrMsgIsDisplayed();

                    Thread.Sleep(200);
                    paymentSelectionPage.ClickOnMailChkBox();



                    paymentSelectionPage.ClickOnPrivacyPolicyChkBox();

                    //paymentSelectionPage.IsEmailInputFieldEqualVWID(); // TODO can't get the text from input email input field



                    paymentSelectionPage.ClickOnDirectPaymentBtn();


                }).InitPage(new PKWAdditionalServiceSummaryPage(GetDriver()), additionalService =>
                {
                    additionalService.ClickOnAbholungDeliveryMethod();
                    additionalService.FillLicensePlateFields();
                    additionalService.ChooseTheDateFromCelendarAbholung();
                    additionalService.ChooseDeliveryTime();

                    additionalService.ClickOnSubmitBtn();

                }).InitPage(new PKWPaymentPage(GetDriver()), paymentPage =>
                {
                    paymentPage.ChoosePaymentMethodByBankTransfer();
                }).InitPage(new PKWOrderSuccessPage(GetDriver()), orderSuccessPage =>
                {
                    orderSuccessPage.IsOrderSuccessPageDisplayed();
                });
            }
        }
    }
}
