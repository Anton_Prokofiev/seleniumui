﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SeleniumUITestFramework.PagesObjects.NFZ;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Tests
{
    [TestFixture]
    [Category("Smoke NFZ")]
    public class VerifyNFZPrivatDirektbezahlungFlow : MainTestOnlineSales
    {

        [Test(Description = "Verify that user can successfully walk-through Privat/Direktbezahlung flow (already registered user)")]
        [TestCase("?key=REVVMDE5MzcyNTU3MTM%3D", "Chrome")]
        //[TestCase("?key=REVVMDE5MzcyMzYwNTk%3D", "Firefox")]
        public void VerifyThatUserCanWalkThroughPrivatDirektbezahlungFlowRegisteredUser(String key, String browserName)
        {

            DriverSetUp(browserName)
           .InitPage(new NFZAccesPage(GetDriver()), accesspage =>
           {
               accesspage.Open(key);
           })
           .InitPage(new NFZHomePage(GetDriver()), homepage =>
           {

               homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
               homepage.DirectPaymentTileClick();
               homepage.OnlinePurchaseBtnClick(GetDriver());
           })
           .InitPage(new NFZVolkswagenLoginPage(GetDriver()), loginpage =>
           {
               loginpage.Login();
           })
           .InitPage(new NFZRegistrationPage(GetDriver()), registrationpage =>
           {

               registrationpage.ContinueOnlineOrderClick();
           })
           .InitPage(new NFZWelcomePage(GetDriver()), welcomepage =>
           {

               welcomepage.SubmitBtnClick();
           })
           .InitPage(new NFZContactDataPage(GetDriver()), contactDataPage =>
           {

               contactDataPage.SelectPhoneCheckBox(GetDriver());
               contactDataPage.ClearContactFormWithPhoneField(GetDriver());
               contactDataPage.SelectValuesFromDropDownMenu(GetDriver(), "Prof.", "Herr");
               contactDataPage.FillInContactFormWithPhone(GetDriver());
           })
           .InitPage(new NFZPaymentPage(GetDriver()), paymentPage =>
           {
               //Thread.Sleep(3000);
               paymentPage.WaitForPaymentAmountExist();
               paymentPage.SubmitPayment();
           })
           .InitPage(new NFZDeliveryAddressPage(GetDriver()), deliveryPage =>
           {

               deliveryPage.ClearDeliveryForm();
               deliveryPage.FillInDeliveryForm(GetDriver());
               deliveryPage.SubmitForm();

           })
           .InitPage(new NFZOrderOverviewPage(GetDriver()), orderOverviewPage =>
           {
               orderOverviewPage.SubmitDeliveryRemarks();
               
           }
           ).InitPage(new NZFOrderSuccessPage(GetDriver()), orderSuccessPage =>
           {
               orderSuccessPage.IsOrderSuccessPageDisplayed();
               Thread.Sleep(5000);
           }
           );
        }
    }
}
