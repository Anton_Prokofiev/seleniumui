﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using NUnit.Framework.Constraints;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SeleniumUITestFramework.Controls.LightBoxes;
using SeleniumUITestFramework.PagesObjects.NFZ;

namespace SeleniumUITestFramework.Tests
{
    [Category("Smoke NFZ")]
    class VerifyNFZGewerbeDirektbezahlungFlow : MainTestOnlineSales
    {
        [Test(Description = "Verify that user can successfully walk-through Gewerbe/Direktbezahlung flow (already registered user)")]
        [TestCase("?key=REVVMDE5MzcyMzYwNTk%3D", "Chrome")]
        public void VerifyThatUserCanWalkThroughGewerbeDirektbezahlungFlowRegisteredUserNettoPrice(string CarKey, string BrowserName)
        {
            var price = default(string);
            DriverSetUp(BrowserName)
            .InitPage(new NFZAccesPage(GetDriver()), accesspage =>
                {
                    accesspage.Open(CarKey);
                })
            .InitPage(new NFZHomePage(GetDriver()), homepage =>
            {
                homepage.Wait().WaitForReady(GetDriver());
                //                    homepage.CookieNotificationClose();
                homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
                homepage.GewerbeTileClick();
                homepage.OnlinePurchaseBtnClick(GetDriver());
            })
            .InitPage(new NFZVolkswagenLoginPage(GetDriver()), loginpage =>
            {
                loginpage.Login();
            })
            .InitPage(new NFZRegistrationPage(GetDriver()), registrationpage =>
            {
                registrationpage.Wait().WaitForReady(GetDriver());
                registrationpage.ContinueOnlineOrderClick();
            })
            .InitPage(new NFZWelcomePage(GetDriver()), welcomepage =>
            {
                welcomepage.Wait().WaitForReady(GetDriver());
                welcomepage.SubmitBtnClick();
            })
            .InitPage(new NFZContactDataPage(GetDriver()), contactDataPage =>
            {
                contactDataPage.Wait().WaitForReady(GetDriver());
                contactDataPage.SelectPhoneCheckBox(GetDriver());
                contactDataPage.ClearContactFormWithPhoneField(GetDriver());
                Assert.That(contactDataPage.IsFirstNameErrorMsgDisplayed, Is.True, "FirstName error message is not displayed");
                Assert.That(contactDataPage.FirstNameErrorMsg.Text, Is.EqualTo(Resources.FirstNameErrorMsg), "First name error message text is different");
                Assert.That(contactDataPage.IsLastNameErrorMsgDisplayed, Is.True, "LastName error message is not displayed");
                Assert.That(contactDataPage.LastNameErrorMsg.Text, Is.EqualTo(Resources.LastNameErrorMsg), "Last name error message text is different");
                Assert.That(contactDataPage.IsEmailErrorMsgDisplayed, Is.True, "Email error message is not displayed");
                Assert.That(contactDataPage.EmailErrorMsg.Text, Is.EqualTo(Resources.EmailErrorMsg), "Email error message text is different");
                Assert.That(contactDataPage.IsTelephoneErrorMsgDisplayed, Is.True, "Phone number error message is not displayed");
                Assert.That(contactDataPage.TelephoneErrorMsg.Text, Is.EqualTo(Resources.TelephoneNumberErrorMsg), "Telephone error message text is different");
                contactDataPage.SelectValuesFromDropDownMenu(GetDriver(), "Prof.", "Herr");
                contactDataPage.FillInContactFormWithPhone(GetDriver());
            })
            .InitPage(new NFZPaymentPage(GetDriver()), paymentPage =>
            {
                paymentPage.Wait().WaitForReady(GetDriver());
                price = paymentPage.GetPriceValue(GetDriver());
                paymentPage.SubmitPayment();
            })
            .InitPage(new NFZDeliveryAddressPage(GetDriver()), deliveryPage =>
            {
                deliveryPage.Wait().WaitForReady(GetDriver());
                deliveryPage.ClearDeliveryForm();
                deliveryPage.FillInDeliveryForm(GetDriver());
                deliveryPage.OpenBillindAddressModal(GetDriver()).ClearBillingForm(GetDriver()).FillInDeliveryForm(GetDriver());

                Thread.Sleep(2000);
                deliveryPage.SubmitForm();
            })
            .InitPage(new NFZOrderOverviewPage(GetDriver()), orderOverviewPage =>
            {
                orderOverviewPage.Wait().WaitForReady(GetDriver());
                Assert.That(orderOverviewPage.RetrieveContactDataValues(3), Is.EqualTo(Resources.TelephoneNumber), "Telephone number is incorrect");
                Assert.That(orderOverviewPage.RetrieveContactDataValues(1), Is.EqualTo(Resources.Email), "Email address is incorrect");
                Assert.That(orderOverviewPage.RetrievePaymentDataValues(3), Is.EqualTo(price + Resources.NettoPrice), "Car price is not displayed");
                Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(0), Is.EqualTo(Resources.Salutation + Resources.Title + Resources.FirstName + Resources.SecondName),
                    "Salutation, Title, First name or Second name is not correct");
                Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(1), Is.EqualTo(Resources.Street + Resources.HouseNumber),
                    "Street or House number is not correct");
                Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(3), Is.EqualTo(Resources.ZipCode + Resources.City),
                    "ZipCode or City is not correct");
                Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(4), Is.EqualTo(Resources.Land), "Land is not correct");
                Assert.That(orderOverviewPage.RetrieveBillingAddressData(0), Is.EqualTo(Resources.Salutation + Resources.Title + Resources.BillingFirstName + Resources.BillingSecondName),
                    "Salutation, Title, First name or Second name is not correct");
                Assert.That(orderOverviewPage.RetrieveBillingAddressData(1), Is.EqualTo(Resources.BillingStreet + Resources.BillingHouseNumber),
                    "Street or House number is not correct");
                Assert.That(orderOverviewPage.RetrieveBillingAddressData(3), Is.EqualTo(Resources.BillingZipCode + Resources.BillingCity),
                    "ZipCode or City is not correct");
                Assert.That(orderOverviewPage.RetrieveBillingAddressData(4), Is.EqualTo(Resources.Land), "Land is not correct");

                orderOverviewPage.SubmitDeliveryRemarks();
                Thread.Sleep(1000);
            }).InitPage(new NFZConfirmationPage(GetDriver()), confirmationPage =>
                {
                    confirmationPage.Wait().WaitForReady(GetDriver());
                    Assert.That(confirmationPage.RetrievePrice(), Is.EqualTo(price + Resources.NettoPrice),
                        "Car price is not displayed");
                });
        }

        [Test(Description = "Verify that user can successfully walk-through Gewerbe/Direktbezahlung flow (already registered user)")]
        [TestCase("?key=REVVMDE5MzcyNTU3MTM%3D", "Firefox")]
        public void VerifyThatUserCanWalkThroughGewerbeDirektbezahlungFlowRegisteredUserBruttoPrice(string CarKey, string BrowserName)
        {
            var price = default(string);
            DriverSetUp(BrowserName)
                .InitPage(new NFZAccesPage(GetDriver()), accesspage =>
                {
                    accesspage.Open(CarKey);
                })
                .InitPage(new NFZHomePage(GetDriver()), homepage =>
                {
                    homepage.Wait().WaitForReady(GetDriver());
                    //                    homepage.CookieNotificationClose();
                    homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
                    homepage.GewerbeTileClick();
                    homepage.OnlinePurchaseBtnClick(GetDriver());
                })
                .InitPage(new NFZVolkswagenLoginPage(GetDriver()), loginpage =>
                {
                    loginpage.Login();
                })
                .InitPage(new NFZRegistrationPage(GetDriver()), registrationpage =>
                {
                    registrationpage.Wait().WaitForReady(GetDriver());
                    registrationpage.ContinueOnlineOrderClick();
                })
                .InitPage(new NFZWelcomePage(GetDriver()), welcomepage =>
                {
                    welcomepage.Wait().WaitForReady(GetDriver());
                    welcomepage.SubmitBtnClick();
                })
                .InitPage(new NFZContactDataPage(GetDriver()), contactDataPage =>
                {
                    contactDataPage.Wait().WaitForReady(GetDriver());
                    contactDataPage.SelectPhoneCheckBox(GetDriver());
                    contactDataPage.ClearContactFormWithPhoneField(GetDriver());
                    Assert.That(contactDataPage.IsFirstNameErrorMsgDisplayed, Is.True, "FirstName error message is not displayed");
                    Assert.That(contactDataPage.FirstNameErrorMsg.Text, Is.EqualTo(Resources.FirstNameErrorMsg), "First name error message text is different");
                    Assert.That(contactDataPage.IsLastNameErrorMsgDisplayed, Is.True, "LastName error message is not displayed");
                    Assert.That(contactDataPage.LastNameErrorMsg.Text, Is.EqualTo(Resources.LastNameErrorMsg), "Last name error message text is different");
                    Assert.That(contactDataPage.IsEmailErrorMsgDisplayed, Is.True, "Email error message is not displayed");
                    Assert.That(contactDataPage.EmailErrorMsg.Text, Is.EqualTo(Resources.EmailErrorMsg), "Email error message text is different");
                    Assert.That(contactDataPage.IsTelephoneErrorMsgDisplayed, Is.True, "Phone number error message is not displayed");
                    Assert.That(contactDataPage.TelephoneErrorMsg.Text, Is.EqualTo(Resources.TelephoneNumberErrorMsg), "Telephone error message text is different");
                    contactDataPage.SelectValuesFromDropDownMenu(GetDriver(), "Prof.", "Herr");
                    contactDataPage.FillInContactFormWithPhone(GetDriver());
                })
                .InitPage(new NFZPaymentPage(GetDriver()), paymentPage =>
                {
                    paymentPage.Wait().WaitForReady(GetDriver());
                    price = paymentPage.GetPriceValue(GetDriver());
                    paymentPage.SubmitPayment();
                })
                .InitPage(new NFZDeliveryAddressPage(GetDriver()), deliveryPage =>
                {
                    deliveryPage.Wait().WaitForReady(GetDriver());
                    deliveryPage.ClearDeliveryForm();
                    deliveryPage.FillInDeliveryForm(GetDriver());
                    deliveryPage.OpenBillindAddressModal(GetDriver()).ClearBillingForm(GetDriver()).FillInDeliveryForm(GetDriver());
                    Thread.Sleep(2000);
                    deliveryPage.SubmitForm();
                })
    .InitPage(new NFZOrderOverviewPage(GetDriver()), orderOverviewPage =>
    {
        orderOverviewPage.Wait().WaitForReady(GetDriver());
        Assert.That(orderOverviewPage.RetrieveContactDataValues(3), Is.EqualTo(Resources.TelephoneNumber), "Telephone number is incorrect");
        Assert.That(orderOverviewPage.RetrieveContactDataValues(1), Is.EqualTo(Resources.Email), "Email address is incorrect");
        Assert.That(orderOverviewPage.RetrievePaymentDataValues(3), Is.EqualTo(price + Resources.BruttoPriceCommercialPrice), "Car price is not displayed");
        Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(0), Is.EqualTo(Resources.Salutation + Resources.Title + Resources.FirstName + Resources.SecondName),
            "Salutation, Title, First name or Second name is not correct");
        Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(1), Is.EqualTo(Resources.Street + Resources.HouseNumber),
            "Street or House number is not correct");
        Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(3), Is.EqualTo(Resources.ZipCode + Resources.City),
            "ZipCode or City is not correct");
        Assert.That(orderOverviewPage.RetrieveDeliveryAddressData(4), Is.EqualTo(Resources.Land), "Land is not correct");
        Assert.That(orderOverviewPage.RetrieveBillingAddressData(0), Is.EqualTo(Resources.Salutation + Resources.Title + Resources.BillingFirstName + Resources.BillingSecondName),
            "Salutation, Title, First name or Second name is not correct");
        Assert.That(orderOverviewPage.RetrieveBillingAddressData(1), Is.EqualTo(Resources.BillingStreet + Resources.BillingHouseNumber),
            "Street or House number is not correct");
        Assert.That(orderOverviewPage.RetrieveBillingAddressData(3), Is.EqualTo(Resources.BillingZipCode + Resources.BillingCity),
            "ZipCode or City is not correct");
        Assert.That(orderOverviewPage.RetrieveBillingAddressData(4), Is.EqualTo(Resources.Land), "Land is not correct");

        orderOverviewPage.SubmitDeliveryRemarks();
        Thread.Sleep(1000);
    }).InitPage(new NFZConfirmationPage(GetDriver()), confirmationPage =>
    {
        confirmationPage.Wait().WaitForReady(GetDriver());
        Assert.That(confirmationPage.RetrievePrice(), Is.EqualTo(price + Resources.BruttoPriceCommercialPrice),
            "Car price is not displayed");
    });
        }
    }
}
