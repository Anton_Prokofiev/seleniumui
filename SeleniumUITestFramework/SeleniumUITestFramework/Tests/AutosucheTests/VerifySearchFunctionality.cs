﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;






namespace SeleniumUITestFramework.Tests
{  
    public class VerifySearchFunctionality : MainTestOnlineSales
    {

 //       private static string AllureConfigDir = @"C:\Users\prokofiev\Documents\Visual Studio 2015\Projects\SeleniumUITestFramework\SeleniumUITestFramework\SeleniumUITestFramework\allureConfig.json";
        [Test(Description = "Verify that user can init search via Search section ")]
        [Category("Autosuche")]
        [Ignore("In progress of development")]
        public void VerifySearchFunctionalityResult()
        {

           var driver = DriverSetUp("Chrome")          //Fluent interface implementation. 
                .InitPage(new AutosucheLandingPage(GetDriver()),
                    page =>
                    {
                        page.InitAutoSearch("Beetle", "20.000 € / 140 € mtl."); 

                    }).InitPage(
                    new SearchResultPage(GetDriver()),
                    page =>
                    {
                        Assert.That(page.IsSearchResultPageDisplayed, Is.True, "Search result page is not displayed");
                    }); 

           
        }
    }
}
