﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.LightBoxes;
using SeleniumUITestFramework.PagesObjects;

namespace SeleniumUITestFramework.Tests.NFZRegressionTests
{
    [TestFixture]
    [Category("Regression NFZ")]
    class VerifyPrivacyAndTermsOfUseLinks : WebDriverInit
    {
        [Test(Description =
            "Verify that Data protection policy and Terms of Use links are leading to correct pages")]
        public void VerifyThatDataProtectionPolicyAndTermsOfUseLinksAreLeadingToCorrectPagesChrome()
        {
            var driver = DriverInit(new ChromeDriver())
                .InitPage(new NFZAccesPage(base.driver), accesspage =>
                {
                    accesspage.Open("?key=REVVMDE5MzcxMTUxNjM%3D");
                })
                .InitPage(new NFZHomePage(base.driver), homepage =>
                {
                    Thread.Sleep(2000);
                    var LightBox = homepage.OrderOnlineNowBtnClick(base.driver);
                    LightBox.OpenDataProtectionPolicy();
                    var DataProtectionPage = base.driver.SwitchTo().Window(base.driver.WindowHandles.Last()).Url;
                    Assert.That(DataProtectionPage, Is.EqualTo(ConfigurationSettings.AppSettings["NFZDataProtectionPolicyURL"]));
                    base.driver.SwitchTo().Window(base.driver.WindowHandles.First());
                    LightBox.OpenTermsOfUse();
                    var TermsOfUse = base.driver.SwitchTo().Window(base.driver.WindowHandles.Last()).Url;
                    Assert.That(TermsOfUse, Is.EqualTo(ConfigurationSettings.AppSettings["NFZTermsOfUseURL"]));
                    
                });
        }
    }
}
