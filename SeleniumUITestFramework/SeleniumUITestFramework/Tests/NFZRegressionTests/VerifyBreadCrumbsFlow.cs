﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.Controls.BreadCrumbs;
using SeleniumUITestFramework.PagesObjects;

namespace SeleniumUITestFramework.Tests.NFZRegressionTests
{
    [TestFixture]
    [Category("Regression NFZ")]
    class VerifyBreadCrumbsFlow : WebDriverInit
    {
        [Test(Description =
            "Verify BreadCrumb Flow")]
        [TestCase("Chrome")]
        [TestCase("FireFox")]
        public void VerifyBreadCrumbsFlows(string BrowserName)
        {
            DriverInit2(driver, BrowserName)
            .InitPage(new NFZAccesPage(base.driver), accesspage =>
            {
                accesspage.Open("?key=REVVMDE5MzcxMTUxNjM%3D");
            })
            .InitPage(new NFZHomePage(driver), homepage =>
            {
                homepage.Wait().WaitForReady(driver);
                homepage.OrderOnlineNowBtnClick(base.driver).SubmitDataPolicyAgreement();
                homepage.GewerbeTileClick();
                homepage.OnlinePurchaseBtnClick(base.driver);
            })
            .InitPage(new NFZVolkswagenLoginPage(base.driver), loginpage =>
            {
                loginpage.LoginWithParameters(ConfigurationSettings.AppSettings["Login"], ConfigurationSettings.AppSettings["Pwd"]);
                var URL = ConfigurationSettings.AppSettings["NFZContactPageURL"];
                base.driver.Navigate().GoToUrl(URL);
            })
            .InitPage(new NFZContactDataPage(base.driver), contactDataPage =>
            {
                contactDataPage.Wait().WaitForReady(driver);
                BreadCrumbsList TestBreadCrumb = new BreadCrumbsList();
                TestBreadCrumb.Init(base.driver);
                Assert.That(TestBreadCrumb.isValidBreadCrumbs(1), Is.True, "Breadcrumb has incorrect status value");
                contactDataPage.SelectValuesFromDropDownMenu(base.driver, "Prof.", "Herr");
                contactDataPage.ClearContactFormWithoutPhoneField(base.driver);
                contactDataPage.FillInContactDataFormWithParam(ConfigurationSettings.AppSettings["FirstName"],
                    ConfigurationSettings.AppSettings["SecondName"], ConfigurationSettings.AppSettings["SecondUserLogin"]);

            }).InitPage(new NFZPaymentPage(base.driver), paymentPage =>
            {
                paymentPage.Wait().WaitForReady(driver);
                BreadCrumbsList TestBreadCrumb = new BreadCrumbsList();
                TestBreadCrumb.Init(base.driver);
                Assert.That(TestBreadCrumb.isValidBreadCrumbs(2), Is.True, "Breadcrumb has incorrect status value");
                paymentPage.SubmitPayment();

            }).InitPage(new NFZDeliveryAddressPage(base.driver), deliveryPage =>
            {
                deliveryPage.Wait().WaitForReady(driver);
                BreadCrumbsList TestBreadCrumb = new BreadCrumbsList();
                TestBreadCrumb.Init(base.driver);
                Assert.That(TestBreadCrumb.isValidBreadCrumbs(3), Is.True, "Breadcrumb has incorrect status value");
                deliveryPage.ClearDeliveryForm();
                deliveryPage.FillInDeliveryForm(base.driver);
                deliveryPage.SubmitForm();

            }).InitPage(new NFZOrderOverviewPage(base.driver), orderOverviewPage =>
            {
                orderOverviewPage.Wait().WaitForReady(driver);
                BreadCrumbsList TestBreadCrumb = new BreadCrumbsList();
                TestBreadCrumb.Init(base.driver);
                Assert.That(TestBreadCrumb.isValidBreadCrumbs(4), Is.True, "Breadcrumb has incorrect status value");
            });
        }
    }
}
