﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;

namespace SeleniumUITestFramework.Tests.NFZRegressionTests
{
    [TestFixture]
    [Category("Regression NFZ")]
    public class VerifyContactDataForm : MainTestOnlineSales
    {
        [Test(Description =
            "Verify BreadCrumb Flow")]
        [Ignore("In progress of development")]
        [TestCase("Chrome")]
        [TestCase("FireFox")]
        public void VerifyDropDownValues(string BrowserName)
        {
            GetDriver()
                .InitPage(new NFZAccesPage(GetDriver()), accesspae =>
           {
               accesspae.Open("?key=REVVMDE5MzcxMTUxNjM%3D");
           }).InitPage(new NFZHomePage(GetDriver()), homepage =>
           {
               homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
               homepage.GewerbeTileClick();
               homepage.OnlinePurchaseBtnClick(GetDriver());

           }).InitPage(new NFZVolkswagenLoginPage(GetDriver()), loginpage =>
           {

           }).InitPage(new NFZContactDataPage(GetDriver()), contactdatapage =>
           {

           });
        }

        private object DriverInit2(RemoteWebDriver driver, string browserName)
        {
            throw new NotImplementedException();
        }
    }
}