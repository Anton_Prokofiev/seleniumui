﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;

namespace SeleniumUITestFramework.Tests.NFZRegressionTests
{
    [TestFixture]
    [Category("Regression NFZ")]
    class VerifyDigitalSeller : MainTestOnlineSales
    {
        public VerifyDigitalSeller()
        {
        }

        [Test(Description =
            "Verify that digital seller has correct data")]
        [TestCase("?key=REVVMDE5MzcxMTUxNjM%3D", "Chrome")]
        public void VerifyThatDigitalSellerHasCorrectData(string CarKey,string BrowserName)
        {
            DriverSetUp(BrowserName)
           .InitPage(new NFZAccesPage(GetDriver()), accesspage =>
           {
               accesspage.Open(CarKey);
           })
           .InitPage(new NFZHomePage(GetDriver()), homepage =>
           {
               Thread.Sleep(2000);
//               homepage.CookieNotificationClose();
               var digitalSellerLightBox = homepage.OpenDigitalSellerLightBox(GetDriver());
               
               Assert.That(digitalSellerLightBox.GetPhoneValue(), Is.EqualTo(Resources.DigitalSellerPhone),
               "Phone is not correct");
           });
        }
    }
}
