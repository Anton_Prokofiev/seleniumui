﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUITestFramework.Controls;
using SeleniumUITestFramework.PagesObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SeleniumUITestFramework.Controls.LightBoxes;
using SeleniumUITestFramework.PagesObjects.NFZ;

namespace SeleniumUITestFramework.Tests
{
    [TestFixture]
    [Category("Regression NFZ")]
    class VerifyDeliveryForm : MainTestOnlineSales
    {
        [Test(Description =
            "Verify that delivery form has contact predefined values")]
        public void VerifyThatDeliveryFormHasContactPredefinedValuesChrome()
        {
            var driver = DriverSetUp()
                .InitPage(new NFZAccesPage(GetDriver()), accesspage =>
                {
                    accesspage.Open("?key=REVVMDE5MzcxMTUxNjM%3D");
                })
                .InitPage(new NFZHomePage(GetDriver()), homepage =>
                {
                    Thread.Sleep(2000);
                    homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
                    homepage.GewerbeTileClick();
                    homepage.OnlinePurchaseBtnClick(GetDriver());
                })
                .InitPage(new NFZVolkswagenLoginPage(GetDriver()), loginpage =>
                {
                    loginpage.LoginWithParameters(ConfigurationSettings.AppSettings["SecondUserLogin"],
                        ConfigurationSettings.AppSettings["SecondUserPwd"]);
                    var URL = ConfigurationSettings.AppSettings["NFZContactPageURL"];
                    GetDriver().Navigate().GoToUrl(URL);
                })
                .InitPage(new NFZContactDataPage(GetDriver()), contactDataPage =>
                {
                    Thread.Sleep(2000);
                    contactDataPage.SelectValuesFromDropDownMenu(GetDriver(), "Prof.", "Herr");
                    contactDataPage.ClearContactFormWithoutPhoneField(GetDriver());
                    contactDataPage.FillInContactDataFormWithParam(ConfigurationSettings.AppSettings["FirstName"],
                        ConfigurationSettings.AppSettings["SecondName"], ConfigurationSettings.AppSettings["SecondUserLogin"]);

                    var URL = ConfigurationSettings.AppSettings["NFZDeliveryAddressPageURL"];
                    GetDriver().Navigate().GoToUrl(URL);
                })
                .InitPage(new NFZDeliveryAddressPage(GetDriver()), deliveryPage =>
                {
                    Thread.Sleep(200);
                    Assert.That(deliveryPage.TitleValue(GetDriver()), Is.EqualTo(Resources.Title.TrimEnd(' ')));
                    Assert.That(deliveryPage.SalutationValue, Is.EqualTo(Resources.Salutation.TrimEnd(' ')));
                    Assert.That(deliveryPage.FirstNameValue, Is.EqualTo(Resources.FirstName.TrimEnd(' ')));
                    Assert.That(deliveryPage.SecondNameValue, Is.EqualTo(Resources.SecondName.TrimEnd(' ')));
                });
        }

        [Test(Description =
            "Verify delivery form validation errors")]
        public void VerifyDeliveryFormValidationErrorsChrome()
        {
            DriverSetUp()
            .InitPage(new NFZAccesPage(GetDriver()), accesspage =>
            {
                accesspage.Open("?key=REVVMDE5MzcxMTUxNjM%3D");
            })
            .InitPage(new NFZHomePage(GetDriver()), homepage =>
            {
                Thread.Sleep(2000);
                homepage.OrderOnlineNowBtnClick(GetDriver()).SubmitDataPolicyAgreement();
                homepage.GewerbeTileClick();
                homepage.OnlinePurchaseBtnClick(GetDriver());
            })
            .InitPage(new NFZVolkswagenLoginPage(GetDriver()), loginpage =>
            {
                loginpage.LoginWithParameters(ConfigurationSettings.AppSettings["SecondUserLogin"],
                    ConfigurationSettings.AppSettings["SecondUserPwd"]);
                var URL = ConfigurationSettings.AppSettings["NFZContactPageURL"];
                GetDriver().Navigate().GoToUrl(URL);
            })
            .InitPage(new NFZContactDataPage(GetDriver()), contactDataPage =>
            {
                var URL = ConfigurationSettings.AppSettings["NFZDeliveryAddressPageURL"];
                GetDriver().Navigate().GoToUrl(URL);
            })
            .InitPage(new NFZDeliveryAddressPage(GetDriver()), deliveryPage =>
            {
                //Thread.Sleep(1000);
                deliveryPage.ClearDeliveryForm();
                Assert.That(deliveryPage.IsCityErrorMsgDisplayed, Is.True, "City error message is not displayed");
                Assert.That(deliveryPage.CityErrorMsg.Value.Text, Is.EqualTo(Resources.CityErrorMsg), "City error message has incorrect text");
                Assert.That(deliveryPage.IsStreetErrorMsgDisplayed, Is.True, "Street error message is not displayed");
                Assert.That(deliveryPage.StreetErrorMsg.Value.Text, Is.EqualTo(Resources.StreetErrorMsg), "Street error message has incorrect text");
                Assert.That(deliveryPage.IsFirstNameErrorMsgDisplayed, Is.True, "FirstName error message is not displayed");
                Assert.That(deliveryPage.FirstNameErrorMsg.Value.Text, Is.EqualTo(Resources.FirstNameErrorMsg), "FirstName error message has incorrect text");
                Assert.That(deliveryPage.IsLastNameErrorMsgDisplayed, Is.True, "LastName error message is not displayed");
                Assert.That(deliveryPage.LastNameErrorMsg.Value.Text, Is.EqualTo(Resources.LastNameErrorMsg), "LastName error message has incorrect text");
                Assert.That(deliveryPage.IsHouseNumberErrorMsgDisplayed, Is.True, "Hause Number error message is not displayed");
                Assert.That(deliveryPage.HouseNumberErrorMsg.Value.Text, Is.EqualTo(Resources.HouseNumberErrorMsg), "Hause Number error message has incorrect text");
                Assert.That(deliveryPage.IsPostalCodeErrorMsgDisplayed, Is.True, "Postal code error message is not displayed");
                Assert.That(deliveryPage.PostalCodeErrorMsg.Value.Text, Is.EqualTo(Resources.PostalCodeErrorMsg), "Postal Code error message has incorrect text");
            });
        }
    }
}