﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Slava_test.Test;
using SeleniumUITestFramework.Controls.LightBoxes;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace SeleniumUITestFramework.Slava_test.Pages
{
    class MainPage
    {
        protected int DefaultWaitTime = 10;
        private MainTest mainTest;
        protected RemoteWebDriver driver;
        public MainPage(RemoteWebDriver driver)
        {
            this.driver = driver;
        }
        public IWebElement getElement(By locator, String errMsg)
        {

            IWebElement webElement = driver.FindElement(locator);

            return webElement;
        }
        public ReadOnlyCollection<IWebElement> getElements(By locator, String errMsg)
        {
            try
            {
                ReadOnlyCollection<IWebElement> webElements = driver.FindElements(locator);

                return webElements;
            }
            catch (NoSuchElementException e)
            {

                Assert.Fail(errMsg + " | " + e.Message, e);
                return null;
            }

        }
        public int getElementsCount(By locator, String errMsg)
        {
            ReadOnlyCollection<IWebElement> webElements = getElements(locator, errMsg);

            return webElements.Count;
        }
        protected void isDisplayed(By locator, int waitSec, String errMsg)
        {

            try
            {
                //IWebElement element = GetElement(locator, errMsg);
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait.Until(ExpectedConditions.ElementIsVisible(locator));

                Assert.That(driver.FindElement(locator).Displayed, errMsg);
            }
            catch (NoSuchElementException e)
            {

                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            finally
            {

            }
        }
        protected void lazyClickOnElement(By locator, int waitSec, String errMsg)
        {

            try
            {

                Lazy<IWebElement> element = new Lazy<IWebElement>(() => getElement(locator, errMsg));
                //IWebElement element = GetElement(locator, errMsg);
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait.Until(ExpectedConditions.ElementIsVisible(locator));

                Assert.That(element.Value.Enabled, errMsg);
                element.Value.Click();
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            finally
            {

            }
        }

        protected void clickOnElement(By locator, int waitSec, String errMsg)
        {



            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
            wait.Until(ExpectedConditions.ElementToBeClickable(locator));
            IWebElement element = getElement(locator, errMsg);
            Assert.That(element.Enabled, errMsg);
            try
            {
                element.Click();
            }




            catch (NoSuchElementException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (ElementNotInteractableException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            catch (WebDriverException e)
            {
                try
                {
                    IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                    js.ExecuteScript("arguments[0].click();", element);
                }
                catch (WebDriverException err)
                {
                    Assert.Fail(errMsg + " | " + e.Message, err);
                }

            }
            finally
            {

            }
        }
        protected void clearAndFillField(By locator, String inputText, int waitSec, String errMsg)
        {

            try
            {

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait.Until(ExpectedConditions.ElementIsVisible(locator));
                IWebElement element = getElement(locator, errMsg);

                Assert.That(element.Displayed, errMsg);

                element.Clear();
                element.SendKeys(inputText);
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
            }
            finally
            {

            }
        }
        protected String getTextFromElement(By locator, int waitSec, String errMsg)
        {

            try
            {
                WebDriverWait wait1 = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait1.Until(ExpectedConditions.ElementIsVisible(locator));

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait.Until<bool>(ctx => ctx.FindElement(locator).Text != "");
                IWebElement element = getElement(locator, errMsg);

                // Assert.That(element.Displayed, errMsg);



                //String elementText = Regex.Split(element.Text, "\r\n")[1];
                return element.Text;
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
                return "";
            }
        }
        protected float getFloatFromElementText(By locator, int waitSec, String errMsg)
        {

            try
            {
                WebDriverWait firstWait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                firstWait.Until(ExpectedConditions.ElementIsVisible(locator));

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                wait.Until<bool>(ctx => ctx.FindElement(locator).Text != "");
                IWebElement element = getElement(locator, errMsg);

                Assert.That(element.Displayed, errMsg);


                String resultWithComma = Regex.Replace(element.Text, @"[^\d\,]", "");


                float Float = float.Parse(resultWithComma);

                //String elementText = Regex.Split(element.Text, "\r\n")[1];
                return Float;
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
                return 0;
            }
        }



        protected String getMoreThanZeroIntegerFromElement(By locator, int waitSec, String errMsg)
        {

            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));


                wait.Until<bool>(ctx => ctx.FindElement(locator).Text != null);



                //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSec));
                //wait.Until(ExpectedConditions.ElementIsVisible(locator));
                IWebElement element = getElement(locator, errMsg);

                Assert.That(element.Displayed, errMsg);



                //String elementText = Regex.Split(element.Text, "\r\n")[1];
                String elementText = element.Text;
                return element.Text;
            }
            catch (WebDriverException e)
            {
                Assert.Fail(errMsg + " | " + e.Message, e);
                return "";
            }
        }


        public int randomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }


    }


}
