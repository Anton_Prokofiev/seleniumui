﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Slava_test.Test
{
    [TestFixture]
    public class MainTest
    {
        private RemoteWebDriver driver;
        private Boolean IsLocalRunning = true;
        private String SelenoidUrl = "http://192.168.44.128";
        private String BrowserType = "chrome";
        private String BrowserVersion = "72.0";

        public RemoteWebDriver GetDriver()
        {
            return this.driver;
        }
        public void SetDriver(RemoteWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException(nameof(driver));
            }

            this.driver = driver;

        }



        public RemoteWebDriver DriverSetUp(String browserName = "Chrome")
        {
            RemoteWebDriver driver;

            Console.WriteLine("BrowserName = " + browserName);
            if (this.IsLocalRunning)
            {

                switch (browserName)
                {
                    case "Chrome":
                        driver = new ChromeDriver();
                        SetDriver(driver);
                        break;
                    case "Firefox":
                        driver = new FirefoxDriver();
                        SetDriver(driver);
                        break;
                    default:
                        driver = new ChromeDriver();
                        break;
                }

                return driver;

            }
            else
            {
                var capabilities = new DesiredCapabilities(BrowserType, BrowserVersion, new Platform(PlatformType.Any));
                capabilities.SetCapability("screenResolution", "1920x1080");
                //capabilities.SetCapability("enableVNC", false);
                driver = new RemoteWebDriver(new Uri(SelenoidUrl + ":7777/wd/hub/"), capabilities);
                SetDriver(driver);
                return driver;
            }
            GetDriver().Manage().Window.Size = new Size(1920, 1080);
            
        }

        [TearDown]
        public void TearDown()
        {
            ((ITakesScreenshot)GetDriver()).GetScreenshot().SaveAsFile("C:/Users/semenov/Documents/projects/selenium-ui-tests/Test.png", ScreenshotImageFormat.Png);

            GetDriver().Quit();
        }
    }
}
