﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Controls
{
    // This IPage interface helps to initialize any Page using extended method in IWebDriver
    interface IPage
    {
 //       void Open(RemoteWebDriver driver); 
        void Init(RemoteWebDriver driver);
    }
}
