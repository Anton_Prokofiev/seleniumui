﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Controls.LightBoxes
{
    class LightBox : ILightBoxControl
    {
        private Lazy<IWebElement> DataPolicyAgreementChkBox;
        private Lazy<IWebElement> DataPolicyAgreementBtn;
        private Lazy<IWebElement> FirstNameTextField;
        private Lazy<IWebElement> SecondNameTextField;
        private Lazy<IWebElement> StreetTextField;
        private Lazy<IWebElement> HouseNumberTextField;
        private Lazy<IWebElement> ZipCodeTextField;
        private Lazy<IWebElement> CityTextField;
        private Lazy<IWebElement> SubmitBtn;
        private Lazy<IWebElement> CloseBtn;
        private Lazy<IWebElement> DataProtectionPolicyLink;
        private Lazy<IWebElement> TermsOfUseLink;
        private Lazy<IWebElement> DigitalSellerPhone;
        private Lazy<IList<IWebElement>> TitleDropDownList;
        private Lazy<IList<IWebElement>> SalutationDropDownList;
        private Lazy<IWebElement> TitleDropDownbtn;
        private Lazy<IWebElement> SalutationDropDownbtn;
        private Lazy<IWebElement> PhoneField;



        public void Init(RemoteWebDriver driver)
        {
            TitleDropDownbtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#title"));
            TitleDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector("#title > option"));
            SalutationDropDownbtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#salutation"));
            SalutationDropDownList = new Lazy<IList<IWebElement>>(() => driver.FindElementsByCssSelector("#salutation > option"));
            DataPolicyAgreementChkBox = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-checkbox__icon"));
            DataPolicyAgreementBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-button--primary"));
            FirstNameTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--firstName"));
            SecondNameTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--lastName"));
            StreetTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--street"));
            HouseNumberTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--houseNumber"));
            ZipCodeTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--zipCode"));
            CityTextField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--city"));
            SubmitBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-button--primary"));
            CloseBtn = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open .os-icon--extra-small"));
            DataProtectionPolicyLink = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-checkbox__label > :nth-child(1)"));
            TermsOfUseLink = new Lazy<IWebElement>(() => driver.FindElementByCssSelector(".os-checkbox__label > :nth-child(2)"));
            DigitalSellerPhone = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-headline.os-headline--2"));
        }

        public string GetPhoneValue()
        {
            return DigitalSellerPhone.Value.Text;
        }
        public void SubmitDataPolicyAgreement()
        {
            DataPolicyAgreementChkBox.Value.Click();
            DataPolicyAgreementBtn.Value.Click();
        }

        public void CloseReservedCarDialog()
        {
            CloseBtn.Value.Click();
        }
        public void OpenDataProtectionPolicy()
        {
            DataProtectionPolicyLink.Value.Click();
        }

        public void OpenTermsOfUse()
        {
            TermsOfUseLink.Value.Click();
        }


        public LightBox ClearBillingForm(RemoteWebDriver driver)
        {
            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(Keys.Control + "a");
            FirstNameTextField.Value.SendKeys(Keys.Delete);
            SecondNameTextField.Value.Click();
            SecondNameTextField.Value.SendKeys(Keys.Control + "a");
            SecondNameTextField.Value.SendKeys(Keys.Delete);
            StreetTextField.Value.Click();
            StreetTextField.Value.SendKeys(Keys.Control + "a");
            StreetTextField.Value.SendKeys(Keys.Delete);
            HouseNumberTextField.Value.Click();
            HouseNumberTextField.Value.SendKeys(Keys.Control + "a");
            HouseNumberTextField.Value.SendKeys(Keys.Delete);
            ZipCodeTextField.Value.Click();
            ZipCodeTextField.Value.SendKeys(Keys.Control + "a");
            ZipCodeTextField.Value.SendKeys(Keys.Delete);
            CityTextField.Value.Click();
            CityTextField.Value.SendKeys(Keys.Control + "a");
            CityTextField.Value.SendKeys(Keys.Delete);
            return driver.CreateLightBox<LightBox>();
        }
        public void FillInDeliveryForm(RemoteWebDriver driver)
        {
            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingFirstName"]);
            SecondNameTextField.Value.Click();
            SecondNameTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingSecondName"]);
            StreetTextField.Value.Click();
            StreetTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingStreet"]);
            HouseNumberTextField.Value.Click();
            HouseNumberTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingHouseNumber"]);
            ZipCodeTextField.Value.Click();
            ZipCodeTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingZipCode"]);
            CityTextField.Value.Click();
            CityTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingCity"]);
            //            bool isDisplayed = driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--mobile").Displayed;
            try
            {
                var b = driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--mobile").Displayed;
                PhoneField = new Lazy<IWebElement>(() => driver.FindElementByCssSelector("#os-dialog-portal .os-dialog--is-open #Input--mobile"));
                PhoneField.Value.Click();
                PhoneField.Value.SendKeys(ConfigurationSettings.AppSettings["PhoneNumber"]);
            }
            catch (NoSuchElementException e)
            {
                SubmitBtn.Value.Click();
            }
        }

        public void PKWFillInDeliveryForm(RemoteWebDriver driver)
        {



            FirstNameTextField.Value.Click();
            FirstNameTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingFirstName"]);
            SecondNameTextField.Value.Click();
            SecondNameTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingSecondName"]);
            StreetTextField.Value.Click();
            StreetTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingStreet"]);
            HouseNumberTextField.Value.Click();
            HouseNumberTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingHouseNumber"]);
            ZipCodeTextField.Value.Click();
            ZipCodeTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingZipCode"]);
            CityTextField.Value.Click();
            CityTextField.Value.SendKeys(ConfigurationSettings.AppSettings["BillingCity"]);


            SalutationDropDownbtn.Value.Click();
            Lazy<IWebElement> SalutationOption = new Lazy<IWebElement>(() => driver.FindElementByXPath("//select[@id='salutation']/option[2]"));
            SalutationOption.Value.Click();

            Lazy<IWebElement> TitleOption = new Lazy<IWebElement>(() => driver.FindElementByXPath("//select[@name='title']/option[2]"));
            TitleOption.Value.Click();

            SubmitBtn.Value.Click();
        }
    }
}
