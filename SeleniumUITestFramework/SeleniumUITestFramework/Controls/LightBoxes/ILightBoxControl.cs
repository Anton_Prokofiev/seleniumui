﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Controls.LightBoxes
{
    interface ILightBoxControl
    {
        void Init(RemoteWebDriver driver);
    }
}
