﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using SeleniumUITestFramework.Controls.LightBoxes;

namespace SeleniumUITestFramework.Controls
{
    static class IWebDriverExtentions
    {
        public static RemoteWebDriver InitPage<TPage>(this RemoteWebDriver driver, TPage page, Action<TPage> pageInit) // where TPage:IPage// This method helps Initialize any page.
        {           
//            page.Open(driver);
            //page.Init(driver);
            pageInit(page);
            return driver;
        }

        public static TBox CreateLightBox<TBox>(this RemoteWebDriver driver) where TBox : ILightBoxControl, new()
// This method helps Create Instance of LightBox.
        {
            var lightBox = new TBox();
            lightBox.Init(driver);
            return lightBox;
        }
    }
}
