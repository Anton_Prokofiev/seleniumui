﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Controls
{
  public class WebDriverInit
    {
        public RemoteWebDriver driver;
        public RemoteWebDriver DriverInit(RemoteWebDriver driver)
        {
            this.driver = driver;
            return this.driver;
        }

        public RemoteWebDriver DriverInit2(RemoteWebDriver driver, string BrowserName)
        {
            if (BrowserName == "Chrome")
            {
               var webDriver = new ChromeDriver();
               this.driver = webDriver;
               return webDriver;
            }
            if (BrowserName == "FireFox")
            {
               var webDriver = new FirefoxDriver();
               this.driver = webDriver;
               return webDriver;
            }
            return null;
        }

        [TearDown]
        public void BrowserClose()
        {
             driver.Close();
             driver.Quit();
        }
    }
}
