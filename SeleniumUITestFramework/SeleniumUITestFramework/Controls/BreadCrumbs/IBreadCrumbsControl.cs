﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;

namespace SeleniumUITestFramework.Controls.BreadCrumbs
{
    interface IBreadCrumbsControl
    {
        void Init(RemoteWebDriver driver);
    }
}
